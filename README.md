# Reproducible Track Profile Pipeline (RTP2)

A collection of notebook to illustrate how to orchestrate the [Reproducible Track Profile Pipeline](https://www.nature.com/articles/s41598-023-32924-7) in Flywheel.